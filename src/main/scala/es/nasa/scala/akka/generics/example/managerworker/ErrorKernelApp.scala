package es.nasa.scala.akka.generics.example.managerworker

import akka.actor.typed.ActorSystem

object ErrorKernelApp extends App {

  val system: ActorSystem[Guardian.Command] = ActorSystem(Guardian(), "error-kernel")
      system ! Guardian.Start(List("one", "two"))

}
