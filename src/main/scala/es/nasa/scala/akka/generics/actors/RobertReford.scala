package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object RobertReford {

  def apply(): Behavior[String] = Behaviors.receiveMessage(onMessage)

  def onMessage(): String => Behavior[String] = mensaje => {
    println(s"Oferta Pelicula $mensaje")
    Behaviors.same[String]
  }

}
