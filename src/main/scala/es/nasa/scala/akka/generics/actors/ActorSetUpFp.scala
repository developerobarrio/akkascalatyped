package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}

object ActorSetUpFp {

  sealed trait Command
  final case object Increase extends Command

  def apply(init: Int, max: Int): Behavior[Command] =
    Behaviors.setup(  context => Behaviors.receiveMessage {
                              case Increase =>
                                context.log.info(s"increasing to $init")
                                apply(init + 1, max)
                                if (init > max) {
                                  context.log.info(s"I'm overloaded. Counting '$init' while max is '$max")
                                  Behaviors.stopped
                                }
                                Behaviors.same
                            })
//  Behaviors.setup(context => Behaviors.receiveMessage(onMessages(context, init, max)))
//
//  def onMessages(context: ActorContext[Command], init: Int, max: Int): Command => Behavior[Command] = {
//
//                case Increase =>
//                  context.log.info(s"increasing to $init")
//                  apply(init + 1, max)
//                  if (init > max) {
//                    context.log.info(s"I'm overloaded. Counting '$init' while max is '$max")
//                    Behaviors.stopped
//                  }
//                  Behaviors.same
//  }
}