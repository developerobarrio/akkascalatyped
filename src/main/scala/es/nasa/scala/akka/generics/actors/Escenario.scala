package es.nasa.scala.akka.generics.actors

import akka.actor.typed.ActorSystem


object Escenario {


  def lanzador: Unit = {
    val escenario = ActorSystem[String](Person(), "Escenario")
    escenario ! "El Padrino"
    escenario ! "El Padrino II"
    escenario ! "El Padrino III"
    Thread.sleep(1000)
    escenario.terminate()
  }

  def personalidad: Unit = {
    val escenario = ActorSystem[String](Person(), "Escenario")
    escenario ! "El Padrino"
    escenario ! "akka is complicate"
    escenario ! "El Padrino III"
    escenario ! "akka is good"
    escenario ! "akka is my favorite framework"
    Thread.sleep(1000)
    escenario.terminate()
  }

  def main(args: Array[String]): Unit = {
    personalidad
  }

}
