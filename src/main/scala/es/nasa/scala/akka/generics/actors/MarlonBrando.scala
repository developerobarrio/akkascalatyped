package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object MarlonBrando {

  def apply(): Behavior[String] = Behaviors.setup {
    context => context.log.info("SetUp Actor")
               Behaviors.receiveMessage(onMessage)
  }

  def onMessage(): String => Behavior[String] = mensaje => {
    println(s"Oferta Pelicula $mensaje")
    Behaviors.same[String]
  }

}
