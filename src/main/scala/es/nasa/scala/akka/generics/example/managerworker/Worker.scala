package es.nasa.scala.akka.generics.example.managerworker

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}

object Worker {
  sealed trait Command
  final  case class Do(replyTo: ActorRef[Worker.Response], task: String)  extends Command
  sealed trait Response
  final  case class Done(task: String) extends Response
  def apply(): Behavior[Command] =
               Behaviors.receive {
                    (context,message) => message match {
                                                 case Do(replyTo, task) =>
                                                      context.log.info(s"'${context.self.path}'. Done with '$task'")
                                                      replyTo ! Worker.Done(task)
                                                      Behaviors.stopped
                                                 }
               }
}