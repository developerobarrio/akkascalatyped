package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}

object ActorTypeFp {

  sealed trait Command

  final case object Increase extends Command

  def apply(init: Int, max: Int): Behavior[Command] =
    Behaviors.receive((context,msg) => onMessages(context, msg, init, max))

  def onMessages(context: ActorContext[Command],
                 msg: Command,
                 init: Int,
                 max: Int): Behavior[Command] = {

    msg match {
      case Increase =>
        context.log.info(s"increasing to $init")
        apply(init + 1, max)
        if (init > max) {
          context.log.info(s"I'm overloaded. Counting '$init' while max is '$max")
          Behaviors.stopped
        }
        Behaviors.same
    }
  }
}







