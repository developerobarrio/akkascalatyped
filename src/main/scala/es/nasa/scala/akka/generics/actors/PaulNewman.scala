package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}

object PaulNewman {


  def apply(): Behavior[String] = Behaviors.receive(receive())

  private def receive(): (ActorContext[String], String) => Behavior[String] = {
    (context, message) =>
      context.log.info(s"Mensajes Multiple  $message")
      Behaviors.same[String]
  }
}
