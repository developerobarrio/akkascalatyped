package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import es.nasa.scala.akka.generics.actors.ActorTypeOo.{Command, Increase}


object ActorTypeOo {

  sealed trait Command

  final case object Increase extends Command

  def apply(init: Int, max: Int): Behavior[Command] =
    Behaviors.setup((context) => new ActorTypeOo(init, max, context))
}

private class ActorTypeOo(init: Int,
                          max: Int,
                          context: ActorContext[Command]) extends AbstractBehavior[Command](context) {
  var current = init

  override def onMessage(message: Command): Behavior[Command] = {
    var behavior = Behaviors.same[Command]
    message match {
      case Increase =>
        current += 1
        context.log.info(s"increasing to $current")
        if (current > max) {
          context.log.info(
            s"I'm overloaded. Counting '$current' while max is '$max")
          behavior = Behaviors.stopped
        }
        behavior
    }
  }
}
