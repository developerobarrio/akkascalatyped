package es.nasa.scala.akka.generics.actors


import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object Person {

  def apply(): Behavior[String] = happy()

  def happy(): Behavior[String] = Behaviors.receive {
    (context, message) =>
      message match {
        case "akka is complicate" =>
          context.log.info(s"Happy with my present  $message")
          sad()
        case _ => context.log.info(s"Happy with my present  $message")
          Behaviors.same
      }
  }

  def sad(): Behavior[String] = Behaviors.receive {
    (context, message) =>
      message match {
        case "akka is good" =>
          happy()
        case _ => context.log.info(s"So Boring with my present  $message")
          Behaviors.same
      }
  }
}
