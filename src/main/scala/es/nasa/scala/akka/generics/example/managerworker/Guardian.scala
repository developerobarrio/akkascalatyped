package es.nasa.scala.akka.generics.example.managerworker

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}

object Guardian {

  sealed trait Command
  case class Start(tasks: List[String]) extends Command

  def apply(): Behavior[Command] = Behaviors.setup {
      context => context.log.info("setting up")
      val manager: ActorRef[Manager.Command] = context.spawn(Manager(), "manager-alpha")
                   Behaviors.receiveMessage { case Start(tasks) => manager ! Manager.Delegate(tasks)
                                                      Behaviors.same  }
  }
}