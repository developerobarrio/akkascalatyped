package es.nasa.scala.akka.generics.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}

object TonSelleck {

  // the “messages” this actor can respond to
  sealed trait Message
  final case object Hello extends Message

  def apply(): Behavior[Message] =  Behaviors.setup(setUp)

  // the factory method
  val setUp = (con: ActorContext[Message]) => Behaviors.receiveMessage(onMessages(con))

  private def onMessages(con: ActorContext[Message]): Message => Behavior[Message] = {
    case Hello => println("Hi, I’m Tom.")
                  Behaviors.same
  }

}

